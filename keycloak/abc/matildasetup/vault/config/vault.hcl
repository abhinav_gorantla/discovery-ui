storage "file" {
	path    = "./vault/file"
	node_id = "node2"
  }
  
listener "tcp" {
	address     = "0.0.0.0:8200"
	tls_disable = 1
  }
  
disable_mlock = true
  
api_addr = "http://0.0.0.0:8200"
//cluster_addr = "https://0.0.0.0:8200"
ui = true
default_lease_ttl = "168h",       
#max_lease_ttl = "0h",
