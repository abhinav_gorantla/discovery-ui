vault operator init > /file/vault-keys-token
#echo "Unseal Vault"
vault operator unseal $(sed -n '1p' /file/vault-keys-token | awk '{print $4}')
vault operator unseal $(sed -n '2p' /file/vault-keys-token | awk '{print $4}')
vault operator unseal $(sed -n '3p' /file/vault-keys-token | awk '{print $4}')
#echo "Vault Token Login"
VAULT_TOKEN=$(grep Root /file/vault-keys-token | awk '{print $4}')
vault login $VAULT_TOKEN
vault secrets enable -path=oci/ -version 2 kv
echo "Vault Path enabled for OCI"
